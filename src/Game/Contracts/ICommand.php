<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface ICommand
{
    public function execute(string $arg = '');
}