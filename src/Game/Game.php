<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $player = new Ships\PirateShip($this->random);

        $harborShip = new Ships\HarborShip($this->random);
        
        $commandInvoker = new Commands\Invoker($player, $harborShip);

        while (1) {

            $input = trim($reader->read());

            if ($input == 'exit') {
                $writer->writeln('Game over!');
                exit;    
            }
            $commandInvoker->submit($input);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
    }
}
