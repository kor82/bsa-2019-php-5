<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship, Harbor};

class SetSailCommand implements ICommand
{

    protected $writer;
    protected $player;
    protected $harborShip;    

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;
    }

    public function execute(string $direction = '')
    {
        $currentHarbor = Harbor::DETAILS[$this->player->currentHarbor];

        $availableDirections = array_keys($currentHarbor['direction']);

        if (!in_array($direction, ['north', 'west', 'east', 'south'])) {
            return $this->writer->writeln(Messages::errors('incorrect_direction_command'));
        }

        try {
            if (in_array($direction, $availableDirections)) {

                $newHarborNumber = $currentHarbor['direction'][$direction];

                $this->player->currentHarbor = $newHarborNumber;

                if ($newHarborNumber !== 1) {

                    $harborShipType = Harbor::DETAILS[$newHarborNumber]['ship'];

                    $this->harborShip->setType($harborShipType)
                                     ->setHarbor($newHarborNumber); 
                } else {

                    if ($this->player->health < 60) {
                        $this->player->restoreHealth();
                        $message = Messages::piratesHarbor();
                    } else {
                        $message = Messages::piratesHarborWithGoodHealth();
                    }

                    return $this->writer->writeln($message);

                }

                return $this->writer->writeln(Messages::harbor($newHarborNumber));

            } else {

                $message = Messages::errors('incorrect_direction');

                throw new \Exception($message);
            }
        } catch (\Exception $e) {

            $this->writer->writeln($e->getMessage());

        }

    }
}