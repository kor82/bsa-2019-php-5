<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship, Harbor};

class DrinkCommand implements ICommand
{

    protected $writer;
    protected $player;
    protected $harborShip;

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;
    }

    public function execute(string $arg = '')
    {   

        if ($this->player->hasRum()) {

            $this->player->health += 30;

            $rumKey = array_search('🍾', $this->player->hold);

            unset($this->player->hold[$rumKey]);
            
            return $this->writer->writeln(Messages::drink($this->player->health));
        }     

        return $this->writer->writeln("You don`t have rum \n");

    }

}