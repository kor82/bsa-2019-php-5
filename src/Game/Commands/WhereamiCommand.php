<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship};

class WhereamiCommand implements ICommand
{

    protected $writer;
    protected $player;    

    public function __construct(Ship $player)
    {
        $this->writer = new CliWriter;

        $this->player = $player;
    }

    public function execute(string $arg = '')
    {
    	$harborNumber = $this->player->currentHarbor;
        
    	return $this->writer->writeln(Messages::whereAmI($harborNumber));
    }
}