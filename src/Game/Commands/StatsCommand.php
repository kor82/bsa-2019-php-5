<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship};

class StatsCommand implements ICommand
{

    protected $writer;
    protected $player;    

    public function __construct(Ship $player)
    {
        $this->writer = new CliWriter;

        $this->player = $player;
    }

    public function execute(string $arg = '')
    {
        $stats = [
            'strength' => $this->player->strength,
            'armour' => $this->player->armour,
            'luck' => $this->player->luck,
            'health' => $this->player->health,
            'hold' => implode(', ',$this->player->hold),
        ];
        
        return $this->writer->writeln(Messages::stats($stats));
    }
}