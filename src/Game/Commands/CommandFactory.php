<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Ships\Ship, Commands\HelpCommand};

class CommandFactory
{
    protected $writer;
    protected $player;
    protected $harborShip;

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;        
    }

    public function create(string $commandName)
    {
        $commandName = $this->formatToCamelCase($commandName);

        $command = 'BinaryStudioAcademy\Game\Commands\\'.$commandName.'Command';

        if (class_exists($command)) {
            return new $command($this->player, $this->harborShip);
        } 
    }   

    protected function formatToCamelCase(string $commandName) : string
    {
        return str_replace('-', '', ucwords($commandName, '-'));
    }
}