<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship, Harbor};

class FireCommand implements ICommand
{

    protected $writer;
    protected $player;
    protected $harborShip;    

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;
    }

    public function execute(string $arg = '')
    {

        if ($this->harborShip->health <= 0) {
            return $this->writer->writeln(Messages::errors('pirate_harbor_fire'));
        }

        $harborShipName = Messages::SHIPS[$this->harborShip->type]['name'];

        $playerDamage = $this->player->shoot($this->harborShip);

        if ($this->harborShip->health <= 0) {

            if ($this->harborShip->type == 'royal') {
                return $this->writer->writeln(Messages::finalWin());
            }

            return $this->writer->writeln(Messages::win($harborShipName));
        }

        $harborShipDamage = $this->harborShip->shoot($this->player);

        if ($this->player->health <= 0) {

            $this->player->goHomeAfterLoss();

            return $this->writer->writeln(Messages::die());
        }

        $message = Messages::fire(
            $harborShipName,
            $playerDamage,
            $this->harborShip->health,
            $harborShipDamage,
            $this->player->health
        );

        return $this->writer->writeln($message);
    }

}