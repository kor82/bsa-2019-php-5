<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship, Harbor};

class AboardCommand implements ICommand
{

    protected $writer;
    protected $player;
    protected $harborShip;

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;
    }

    public function execute(string $arg = '')
    {
        if ($this->player->currentHarbor === 1) {
           return $this->writer->writeln(Messages::errors('pirate_harbor_aboard'));
        }

        if ($this->harborShip->health > 0) {
           return $this->writer->writeln(Messages::errors('aboard_live_ship'));
        }

        return $this->player->aboard($this->harborShip);

    }

}