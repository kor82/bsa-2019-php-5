<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Commands\CommandFactory;
use BinaryStudioAcademy\Game\Ships\Ship;
use BinaryStudioAcademyTests\Game\Messages;

class Invoker
{

	protected $writer;
	protected $commandFactory;
	protected $player;
	protected $harborShip;

	public function __construct(Ship $player, $harborShip)
	{
		$this->writer = new CliWriter;
		$this->player = $player;
		$this->harborShip = $harborShip;
		$this->commandFactory = new CommandFactory($this->player, $this->harborShip);
	}

    public function submit(string $cliInput)
    {
    	$commandName = $this->getCommandNameFromInput($cliInput);

    	$commandArg = $this->getArgsFromInput($cliInput);

    	$command = $this->commandFactory->create($commandName);

        if (NULL === $command) {
            return $this->writer->writeln(Messages::errors('unknown_command'));
        } else {
    	   return $command->execute($commandArg);
        }

    }

    protected function getCommandNameFromInput(string $cliInput): string
    {
    	return explode(' ', $cliInput)[0];
    }

    protected function getArgsFromInput(string $cliInput): string
    {
    	return explode(' ', $cliInput)[1] ?? '';
    }

}