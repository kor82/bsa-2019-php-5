<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship, Harbor};

class BuyCommand implements ICommand
{

    protected $writer;
    protected $player;
    protected $harborShip;

    const GOODS = [
        'strength' => 1,
        'armour' => 1,
        'luck' => 1, 
        'rum' => '🍾'
    ];

    public function __construct(Ship $player, $harborShip)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
        $this->harborShip = $harborShip;
    }

    public function execute(string $goodItem = '')
    {   

        if ($this->player->currentHarbor !== 1) {

            $this->writer->writeln("You can buy in Pirate Harbor only. \n");
            return;
        }

        $availableGoods = $this->getAvailableGoods();

        try {
            if (in_array($goodItem, $availableGoods)) {

                return $this->player->buy($goodItem);

            } else {

                $availableGoods = implode(', ', $availableGoods);

                throw new \Exception("Wrong item. Available: $availableGoods \n");
            }
        } catch (\Exception $e) {

            $this->writer->writeln($e->getMessage());
        }
    }

    protected function getAvailableGoods() : array
    {
        return array_keys(self::GOODS);
    }

}