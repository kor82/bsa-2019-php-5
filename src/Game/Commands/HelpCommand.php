<?php 

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\{Io\CliWriter, Contracts\ICommand, Ships\Ship};

class HelpCommand implements ICommand
{

    protected $writer;
    protected $player;

    public function __construct(Ship $player)
    {
        $this->writer = new CliWriter;
        $this->player = $player;
    }

    public function execute(string $arg = '')
    {
    	return $this->writer->writeln(Messages::help());
    }
}