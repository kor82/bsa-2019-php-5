<?php 

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\PirateShip;
use BinaryStudioAcademyTests\Game\Messages;


abstract class Ship 

{
	const MAX_HOLD = 3;
	
	public $strength; // определяет наносимый урон (1 до 10)
	public $armour;   // защиту к урону (1 - 10)
	public $luck;     // определяет удачу попадания (1-10)
	public $health;   // здоровье корабля (1 - 100)
	public $hold = [];   // трюм корабля для добычи, вместительность: 3 элемента добычи.
	protected $math;
	protected $random;

	protected $writer;	

	public function __construct()
	{
		$this->math = new Math;
        $this->writer = new CliWriter;	
	}


	public function shoot($enemy)
	{
		if (!$this->isNotPirateHarbor()) {
			return;
		}
		
		if ($enemy->health <= 0) {
			$this->writer->writeln("Shot is impossible. \n");
			return;
		}		

		$damage = $this->math->damage($this->strength, $enemy->armour);

		$enemy->health -= $damage;

		return $damage;

	}

    protected function isNotPirateHarbor()
    {
    	try {
    		if ($this->currentHarbor !== 1) {
    			return true;
    		} else {
    			throw new \Exception('This is Pirate Harbor. There is no Harbor Ships here. Sail to other Harbor for battle');
    		}
    	} catch (\Exception $e) {
    		return $this->writer->writeln($e->getMessage());
    	}
    	
    }

}