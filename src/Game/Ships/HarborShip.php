<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Helpers\Random;

class HarborShip extends Ship

{

	const TYPES = ['battle', 'schooner', 'royal'];

	const STATS = [
		
		'battle' => [
		    'strength' => [
		    	'min' => 4,
		    	'max' => 8
		    ],
		    'armour' => [
		    	'min' => 4,
		    	'max' => 8
		    ],
		    'luck' => [
		    	'min' => 4,
		    	'max' => 7
		    ],
		    'health' => 80,
		    'hold' => ['🍾']			
		],

		'schooner' => [
		    'strength' => [
		    	'min' => 2,
		    	'max' => 4
		    ],
		    'armour' => [
		    	'min' => 2,
		    	'max' => 4
		    ],
		    'luck' => [
		    	'min' => 1,
		    	'max' => 4
		    ],
		    'health' => 50,
		    'hold' => ['💰']	
		],

		'royal' => [
		    'strength' => [
		    	'min' => 10,
		    	'max' => 10
		    ],
		    'armour' => [
		    	'min' => 10,
		    	'max' => 10
		    ],
		    'luck' => [
		    	'min' => 10,
		    	'max' => 10
		    ],
		     'health' => 100,
		     'hold' => ['💰', '💰', '🍾'],
		],
	]; 


	public $currentHarbor;
	
	public $type;

	public function __construct(Random $random)
	{
		parent::__construct();
		
	    $this->random = $random;		
	}

	public function setType(string $type)
	{
		$this->validateType($type);

		$this->type = $type;

		$this->setDefaultStats();

		return $this;
	}

	public function setHarbor(int $harbor)
	{
		$this->currentHarbor = $harbor;

		return $this;
	}

	protected function calculateStat(int $min, int $max) : int
	{
		return mt_rand($min, $max);
	}

	protected function validateType(string $type)
	{
		try {
			if (in_array($type, self::TYPES)) {
				return true;
			} else {
				$types = implode(', ', self::TYPES);
				throw new \Exception("wrong type. Available types: $types");
			}
		} catch (\Exception $e) {
			$this->writer->writeln($e->getMessage());
		}
	}

	protected function setDefaultStats()
	{
		$stats = self::STATS[$this->type];

		$this->strength = $this->calculateStat(
			$stats['strength']['min'], 
			$stats['strength']['min']
		);

	    $this->armour = $this->calculateStat(
			$stats['armour']['min'], 
			$stats['armour']['min']
		);

	    $this->luck = $this->calculateStat(
			$stats['luck']['min'], 
			$stats['luck']['min']
		);

	    $this->health = $stats['health'];

	    $this->hold = $stats['hold'];
	}

}