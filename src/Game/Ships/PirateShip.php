<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Harbor;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Ships\HarborShip;
use BinaryStudioAcademyTests\Game\Messages;

class PirateShip extends Ship

{
	public $currentHarbor;
	public $type;

	public function __construct(Random $random)
	{
		parent::__construct();

		$this->strength = 4;
	    $this->armour = 4;
	    $this->luck = 4;
	    $this->health = 60;
	    $this->hold = ['💰'];
	    $this->currentHarbor = 1;
	    $this->type = 'pirate';
	    $this->random = $random;
	}

	public function showStats()
	{
		$hold = implode(', ', $this->hold);
		return <<<EOL
Your current stats:
strength: {$this->strength}
armour:   {$this->armour}
luck:     {$this->luck}
health:   {$this->health} 
hold:     $hold

EOL;
	}

	public function goHomeAfterLoss()
	{
		$this->currentHarbor = 1;

		$this->strength = $this->decrementStat($this->strength);
		$this->armour = $this->decrementStat($this->armour);
		$this->luck = $this->decrementStat($this->luck);

		$this->emptyHold();

		$this->restoreHealth();
	}

	public function decrementStat($stat)
	{
		return $stat = $stat > 0 ? --$stat : 0;
	}

	protected function emptyHold()
	{
		return $this->hold = [];
	}

	public function restoreHealth()
	{
		try {
			if ($this->currentHarbor === 1) {
				return $this->health = 60;
			} else {
				throw new \Exception("Health can be restored in Pirate Harbor only \n");
			}	
		} catch (\Exception $e) {
			$this->writer->writeln($e->getMessage());
		}
	}

	public function aboard(HarborShip $ship)
	{

		foreach ($ship->hold as $item) {

			if (count($this->hold) === self::MAX_HOLD) {
				break;
			}

			$this->hold[] = $item;
		}

		switch ($ship->type) {
			case 'schooner':
				$message = Messages::aboardSchooner();
				break;
			case 'battle':
				$message = Messages::aboardBattleShip();
				break;	
		}

		return $this->writer->writeln($message);
	}

	public function hasRum()
	{
		return in_array('🍾', $this->hold);
	}

	protected function hasGold()
	{
		return in_array('💰', $this->hold);
	}

	public function buy(string $goodItem)
	{
		$isRum = $goodItem == 'rum';

		try {
			if ($this->hasGold()) {
				
				$keyGold = array_search('💰', $this->hold);
				unset($this->hold[$keyGold]);

				if ($isRum) {
					$this->hold[] = '🍾';
					$value = array_count_values($this->hold)['🍾'];
					$message = Messages::buyRum($value);
				} else {
					$qty = ++$this->{$goodItem};
					$message = Messages::buy($goodItem, $qty);
				}

				return $this->writer->writeln($message);

			} else {
				throw new \Exception("You doesn`t have 💰");
			}			
		} catch (\Exception $e) {
			$this->writer->writeln($e->getMessage());
		}
	}

}