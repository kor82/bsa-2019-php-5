<?php

namespace BinaryStudioAcademy\Game;

class Harbor
{

    const DETAILS = [
        1 => [
            'name' => 'Pirates Hardbor',
            'ship' => 'player',
            'direction' => [
                'north' => 4,
                'west'  => 3,
                'south' => 2,
            ]
        ],
        2 => [
            'name' => 'Southhampton',
            'ship' => 'schooner',
            'direction' => [
                'north' => 1,
                'west'  => 3,
                'east'  => 7,
            ]
        ],
        3 => [
            'name' => 'Fishguard',
            'ship' => 'schooner',
            'direction' => [
                'north' => 4,
                'south' => 2,
                'east'  => 1,
            ]
        ],
        4 => [
            'name' => 'Salt End',
            'ship' => 'schooner',
            'direction' => [
                'west'  => 3,
                'south' => 1,
                'east'  => 5,
            ]
        ],
        5 => [
            'name' => 'Isle of Grain',
            'ship' => 'schooner',
            'direction' => [
                'west'  => 4,
                'south' => 7,
                'east'  => 6,
            ]
        ],
        6 => [
            'name' => 'Grays',
            'ship' => 'battle',
            'directions' => [
                'west'  => 5,
                'south' => 8,
            ]
        ],
        7 => [
            'name' => 'Felixstowe',
            'ship' => 'battle',
            'direction' => [
                'north' => 5,
                'west'  => 2,
                'east'  => 8,
            ]
        ],
        8 => [
            'name' => 'London Docks',
            'ship' => 'royal',
            'direction' => [
                'north' => 6,
                'west'  => 7
            ]
        ]
    ];


}
